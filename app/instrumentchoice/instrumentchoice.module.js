var InstrumentChoiceController = require('./instrumentchoice.controller');

angular.module('transcriptapp.instrumentchoice', []);

angular.module('transcriptapp.instrumentchoice')
	.controller('InstrumentChoiceController', ['$scope', InstrumentChoiceController]);
;