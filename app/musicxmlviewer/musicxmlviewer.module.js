var MusicXmlViewerController = require('imports?$=jquery!./musicxmlviewer.controller');
var MusicXmlViewerService = require('./musicxmlviewer.service');
var MusicXmlViewerConfig = require('./musicxmlviewer.config');


angular.module('transcriptapp.musicxmlviewer', ['bootstrap.fileField']);

angular.module('transcriptapp.musicxmlviewer')
	.config(['$compileProvider', MusicXmlViewerConfig]);

angular.module('transcriptapp.musicxmlviewer')
	.factory('TranscriptionService', ['$http', '$alert', MusicXmlViewerService]);
	
angular.module('transcriptapp.musicxmlviewer')
	.controller('MusicXmlViewerController', ['$scope', '$alert', 'TranscriptionService', MusicXmlViewerController]);
	

module.exports = angular.module('transcriptapp.musicxmlviewer');