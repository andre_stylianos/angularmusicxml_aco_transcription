module.exports = function ($http, $alert) {

	var successalert = function () {
		$alert({
			"title": "Transcription",
			"content": "Transcription successful",
			"type": "success",
			"animation": 'am-fade-and-slide-top',
			"duration": 2,
			"placement": 'top-right'
		});
	};

	var erroralert = function () {
		$alert({
			"title": "Transcription",
			"content": "Transcription failed! Valid MusicXML?",
			"type": "error",
			"animation": 'am-fade-and-slide-top',
			"duration": 4,
			"placement": 'top-right'
		});
	};

	return {
		transcribeMusic: function (req) {
			var promise = $http({
				url: "https://aco-musicxml-transcription.herokuapp.com/rest/transcript",
				method: "POST",
				responseType: 'arraybuffer',
				data: req,
				headers: {
					"Accept": "application/octet-stream"
				}
			}).then(function (response) {
				// successalert();
				return response.data;
			}, function (error) {
				erroralert();
			});
			return promise;
		}
	};
};
