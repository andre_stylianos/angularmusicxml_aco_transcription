require('jquery-ui');
var ScoreDiv = require('imports?$=jquery!../../libs/scorediv-latest.js').ScoreLibrary.ScoreDiv;

module.exports = function ($scope, $alert, TranscriptionService) {
	var self = this;
	
	this.selectedInstrument = "violao";
	this.selectedTuning = "eadgbe";
	
	this.fileurl = null;
	
	this.instruments = [{ "value": "violao", "label": "Violão" },
		{ "value": "baixo", "label": "Baixo" }
	];
	
	this.tunings = [{ "value": "eadgbe", "label": "EADGBE" }];
	

	this.alertinfo = {
			"title": "Transcription",
			"content": "Wait a moment, trascription started",
	};
	
	this.displayingtab = true;
	this.tabfordownload = false;
	this.populateTunings = function () {
		if (this.selectedInstrument == "violao") {
			this.tunings = [{ "value": "eadgbe", "label": "EADGBE" }];
			this.selectedTuning = this.tunings[0].value;
		} else {
			this.tunings = [{ "value": "tuning1", "label": "Bass tuning 1" }, { "value": "tuning2", "label": "Bass tuning 2" }];
			this.selectedTuning = this.tunings[0].value;
		}
	};

	var musicxmlRender = function (xml) {
		$('#musicxml-render').empty();
		new ScoreDiv($($('#musicxml-render')[0]), xml, false, false);
		$('#open_file_btn').hide();
		$('#go_url_input').hide();
		$('#go_url_btn').hide();
		$('.ui-button-text').empty();
		$('#page_1st_btn').addClass("btn glyphicon glyphicon-step-backward");
		$('#page_prev_btn').addClass("btn glyphicon glyphicon-backward");
		$('#page_next_btn').addClass("btn glyphicon glyphicon-forward");
		$('#page_nth_btn').addClass("btn glyphicon glyphicon-step-forward");
		$('#standalone_btn').hide();
	};

	$('input').attr("accept", ".xml,.XML,.MXL,.mxl");
	$scope.$watch(function () {
		return self.xmlfile;
	}, function (newValue, oldValue) {
		if (newValue !== oldValue) {
			if ($('.score-div').length == 0) {
				$('#musicxml-render').addClass("score-div");
			}
			musicxmlRender(self.xmlfile);
			self.displayingtab = false;
			self.tabfordownload = false;

		}
	});

	this.doTranscribe = function () {
		var reader = new FileReader();
		reader.onload = function (e) {
			var req = {
				"instrument": self.selectedInstrument,
				"tuning": self.selectedTuning,
				"xml": e.target.result
			};
			TranscriptionService.transcribeMusic(req).then(function (data) {
				var file = new Blob([data], { "type": "application/vnd.recordare.musicxml+xml" });
				file.name = "tabbed_" + self.xmlfile.name;
				file.lastModifiedDate = new Date();
				self.fileurl = (window.URL || window.webkitURL).createObjectURL(file);
				self.filename = file.name;
				musicxmlRender(file);
				self.displayingtab = true;
				self.tabfordownload = true;
			});
		};
		reader.readAsText(this.xmlfile);
	};
};
