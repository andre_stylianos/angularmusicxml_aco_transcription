var NavBarController = require('./navbar.controller');


angular.module('transcriptapp.navbar', [])
	.controller('NavBarController', NavBarController);
	
module.exports = angular.module('transcriptapp.navbar');