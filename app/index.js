var angular = require('angular');
require('angular-sanitize');
require('bootstrap/dist/css/bootstrap.min.css');
require('angular-animate');
require('angular-strap/dist/angular-strap.min.js');
require('angular-strap/dist/angular-strap.tpl.min.js');
require('../libs/angular-bootstrap-file-field.min.js');
var $ = require('jquery');
require('jquery-ui');
// require('imports?$=jquery!../libs/scorediv-latest.js');
require('imports?$=jquery!./musicxmlviewer/musicxmlviewer.module.js');
require('./navbar/navbar.module.js');
require('angular-loading-bar');
require('file-loader!./logo.png');
require('./musicxmlviewer/musicxmlviewer.css');
require('./musicxmlviewer/musicxmlviewer.alert.css');
require('./instrumentchoice/instrumentchoice.module.js');
require('file?name=[name].[ext]!./index.html');
angular.module('transcriptapp', ['mgcrea.ngStrap', 'ngSanitize','angular-loading-bar','transcriptapp.musicxmlviewer','transcriptapp.navbar', 'transcriptapp.instrumentchoice']);


//angular.module('transcriptapp.navbar', ['mgcrea.ngStrap.navbar']);
