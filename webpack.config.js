module.exports = {
  context: __dirname + '/app',
  entry: './index.js',
  output: {
    path: __dirname + '/dist',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
    //  {test: /\.js$/, exclude: /node_modules/, loader: "babel"},
      {test: /\.css$/, loader: "style-loader!css-loader" },
      { test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream" },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" },
      { test: /\.png$/, loader: "url-loader?limit=100000" },
      { test: /\.jpg$/, loader: "file-loader" },
      { test: /\.gif$/, loader: "file-loader" }
    ]
  },
  resolve: {
    modulesDirectories: ['node_modules', 'bower_components'],
  },
  devServer: {
    proxy: {
      '/rest*': {
        target: 'http://localhost:8080/aco-musicxml-transcription',
        secure: false,
      }
    }
  }
};
