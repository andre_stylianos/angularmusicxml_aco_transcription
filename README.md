#### This code was developed as part of my undergraduate thesis. There are many areas that can be improved should this be used for further research. This was developed in a learning environment, and I learned Angular as I was coding, so it may not be following all the correct guidelines.

### What is this repository for? ###

* AngularJS client for the MusicXML transcription backend

### How do I get set up? ###

* Clone this repo
* run `npm install`
* Download [angular-bootstrap-file-field.min.js](https://github.com/itslenny/angular-bootstrap-file-field/blob/master/dist/angular-bootstrap-file-field.min.js) and place it in a folder named libs at root dir
* Download [scorediv-latest.js](https://code.google.com/p/score-library/downloads/detail?name=scorediv-latest.js&can=2&q=) and place it in a folder named libs at root dir
* run `npm start` to start the webpack-dev-server
* Start developing! Webpack will watch the source files for changes and update the files on the server at each file save

### Contribution guidelines ###

* Writing tests
* Code review